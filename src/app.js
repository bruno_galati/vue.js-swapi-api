window.onload = function() {
    var app = new Vue({
        el: '#app',
        data: {
            loading: '',
            overlay: '',
            modal: '',
            planets: [],
            planet: [],
            peoples : [],
            people : [],
            peopleFilmTitle : [],
            planetFilmTitle : [],
            peopleHomeworldName :'',
            nextPlanets : '',
            previousPlanets : '',
            nextPeoples : '',
            previousPeoples : '',
            peoplePagination: '',
            planetPagination: ''
        },
        methods: {
            loadPlanets: function(e) {
                app.loading = true;
                axios.get('https://swapi.co/api/planets/')
                    .then(function(response) {
                        app.planets = response.data.results;
                        app.previousPlanets = response.data.previous;
                        app.nextPlanets = response.data.next;
                        app.planetPagination = true;
                        app.loading = false;
                        button = e.target;
                        button.classList.add('hide');
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            loadMorePlanets: function(url) {
                app.loading = true;
                axios.get(url)
                    .then(function(response) {
                       app.planets = response.data.results;
                       app.previousPlanets = response.data.previous;
                       app.nextPlanets = response.data.next;
                       app.loading = false;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            loadPlanet: function(url) {
                app.loading = true;
                axios.get(url)
                    .then(function(response) {
                       app.loading = false;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            loadPeoples: function(e) {
                app.loading = true;
                axios.get('https://swapi.co/api/people/')
                    .then(function(response) {
                        app.peoples = response.data.results;
                        app.previousPeoples = response.data.previous;
                        app.nextPeoples = response.data.next;
                        app.peoplePagination = true;
                        app.loading = false;
                        button = e.target;
                        button.classList.add('hide');                         
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            loadMorePeoples: function(url, e) {
                app.loading = true;
                axios.get(url)
                    .then(function(response) {
                       app.peoples = response.data.results;
                       app.previousPeoples = response.data.previous;
                       app.nextPeoples = response.data.next;
                       app.loading = false;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            loadPeople: function(id) {
                app.loading = true;
                axios.get('https://swapi.co/api/people/' + id)
                    .then(function(response) {
                        app.people = response.data;
                        app.loading = false;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            viewPlanetDetails: function(url) {
                app.loading = true;
                app.modal = true;
                app.overlay = true;
                axios.get(url)
                    .then(function(response) {
                        app.planet = response.data;
                        app.planetFilms = response.data.films;
                        app.loading = false;
                        var modalEl = document.getElementById('modalplanet');
                        modalEl.classList.add('show');
                        if(app.planetFilmTitle.length > 0){   
                            app.planetFilmTitle = [];
                        }
                         for (var i in app.planetFilms) { 
                            axios.get(app.planetFilms[i])
                                .then(function(response) {
                                    app.planetFilmTitle.push(response.data.title);
                                })
                             .catch(function(error) {
                                 console.log(error);
                             });
                         }
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            viewPeopleDetails: function (url) {
                app.loading = true;
                app.modal = true;
                app.overlay = true;
                axios.get(url)
                    .then(function(response) {
                        app.people = response.data;
                        app.peopleFilms = response.data.films;
                        app.peopleHomeworld = response.data.homeworld;
                        app.loading = false;
                        var modalEl = document.getElementById('modalpeople');
                        modalEl.classList.add('show');
                        axios.get(app.peopleHomeworld)
                            .then(function(response){
                                app.peopleHomeworldName = response.data.name;
                            })
                            .catch(function(error){
                                console.log(error);
                            });
                        if(app.peopleFilmTitle.length > 0){   
                            app.peopleFilmTitle = [];
                        }
                        for (var i in app.peopleFilms) {
                           axios.get(app.peopleFilms[i])
                                .then(function(response) {
                                    app.peopleFilmTitle.push(response.data.title);
                                })
                             .catch(function(error) {
                                 console.log(error);
                            });
                        }

                    })
                    .catch(function(error) {
                        console.log(error);
                    });

		    },

        }
    });
}